// define a function
fn greet() {
    println!("Hello, World!");
}

// function to add two numbers
fn _add() {
    let a = 5;
    let b = 10;

    let sum = a + b;

    println!("Sum of a and b = {}", sum);
}

// function with parameters
fn add(a: i32, b: i32) {
    let sum = a + b;

    println!("Sum of a and b = {}", sum);
}

// define an add function that takes in two parameters with a return type
fn add_(a: i32, b: i32) -> i32 {
    let sum = a + b;

    // return a value from the function
    return sum;
}

fn _add_(a: i32, b: i32) -> i32 {
    a + b
}

fn addsub(a: i32, b: i32) -> (i32, i32) {
    return (a + b, a - b);
}

fn calculate_length(s: &String) -> usize {
    return s.len();
}

fn main() {
    // function call
    greet();

    // function call
    _add();
 
    // call add function with arguments
    add(2, 11);

    // function call
    let sum = add_(3, 5);

    println!("Sum of a and b = {}", sum);

    let sum = _add_(1, 2);

    println!("Sum = {}", sum);

    let (sum, diff) = addsub(4, 1);
    println!("Sum = {}, Difference = {}", sum, diff);

    let word = String::from("hello");

    // passing reference of word variable
    let len = calculate_length(&word);

    println!("The length of '{}' is {}.", word, len);
}