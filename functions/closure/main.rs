fn main() {
    // closure that prints a text
    let print_text = || println!("Hello, World!");

    print_text();

    // define a closure and store it in a variable
    let add_one = |x: i32| x + 1;

    // call closure and store the result in a variable
    let result = add_one(2);

    println!("Result = {}", result);

    // define a multi-line closure
    let squared_sum = |x: i32, y: i32| {
        // find the sum of two parameters
        let mut sum: i32 = x + y;

        // find the squared value of the sum
        let mut result: i32 = sum * sum;

        return result;
    };

    // call the closure
    let result = squared_sum(5, 3);

    println!("Result = {}", result);

    let num = 100;

    // A closure that captures the num variable
    let print_num = || println!("Number = {}", num);

    print_num();

    let word = String::from("Hello");
    
    // immutable closure
    let print_str = || {
        println!("word = {}", word);
    };

    // immutable borrow is possible outside the closure
    println!("length of word = {}", word.len());
    
    print_str();

    let mut word = String::from("Hello");
    
    // mutable closure
    let mut print_str = || {
        // value of word is changed here
        word.push_str(" World!");
        println!("word = {}", word);
    };
     
     // cannot immutable borrow because the variable is borrowed as mutable inside the closure
     // println!("length of word = {}", word.len());
    
    print_str();

    // can immutable borrow because the closure has been already used
    println!("length of word = {}", word.len());

    let word = String::from("Hello");

    // immutable closure
    let print_str = || {
        // word variable is moved to a new variable
        let new_word = word;
        println!("word = {}", new_word);
    };

    print_str();

    // cannot immutable borrow because word variable has moved inside closure
    // println!("length of word = {}", word.len());

    // closure to add numbers
    let add_numbers = |a: i32, b: i32| {
        let sum = a + b;
        println!("Sum: {}", sum);
    };

    // closure to square a variable
    let square = |x: i32| { x * x };
    
    // call add_one function with closure as the second argument
    let result = add_one(5, square);
    
    println!("result = {}", result);
}

// function to add numbers
fn add_numbers(a: i32, b: i32) {
    let sum = a + b;
    println!("Sum: {}", sum);
}

// function that takes in a closure
fn add_one<F: Fn(i32) -> i32>(x: i32, f: F) -> i32 {
    return f(x) + 1;
}