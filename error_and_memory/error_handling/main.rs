use std::fs::File;

fn main() {
    // unrecoverable errors
    println!("Hello, World!");

    // Explicitly exit the program with an unrecoverable error
    panic!("Crash");

    let numbers = [1, 2 ,3];

    println!("unknown index value = {}", numbers[3]); // index out of bounds: the length is 3 but the index is 3

    // recoverable errors
    let data_result = File::open("data.txt");

    // using match for Result type // data_file is a Result<T, E>
    let data_file = match data_result {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the data file: {:?}", error),
    };

    println!("Data file", data_file);

    let text = "Hello, World!";

    let character_option = text.chars().nth(15);

    // using match for Option type
    let character = match character_option {
        None => "empty".to_string(),
        Some(c) => c.to_string()
    };

    println!("Character at index 15 is {}", character);
}