fn main() {
    let str = String::from("Hello, World!");

    // Call function with reference String value
    let len = calculate_length(&str);

    println!("The length of '{}' is {}.", str, len);

    let mut str = String::from("Hello");

    // before modifying the string
    println!("Before: str = {}", str);

    // pass a mutable string when calling the function
    change(&mut str);

    // after modifying the string
    println!("After: str = {}", str);

    let mut str = String::from("hello");

    // mutable reference 1
    let ref1 = &mut str;

    // mutable reference 2
    let ref2 = &mut str; // cannot borrow `str` as mutable more than once at a time

    println!("{}, {}", ref1, ref2);
}

// Function to calculate length of a string
// It takes a reference of a String as an argument
fn calculate_length(s: &String) -> usize {
    s.len()
}

fn change(s: &mut String) {
    // push a string to the mutable reference variable
    s.push_str(", World!");
}