fn main() {
    // owner of the String value
    // rule no. 1 
    let fruit1 = String::from("Banana");

    // ownership moves to another variable
    // only one owner at a time
    // rule no. 2
    let fruit2 = fruit1;

    // cannot print variable fruit1 because ownership has moved
    // error, out of scope, value is dropped
    // rule no. 3
    // println!("fruit1 = {}", fruit1);

    // print value of fruit2 on the screen
    println!("fruit2 = {}", fruit2);

    let x = 11;

    // copies data from x to y
    // ownership rules are not applied here 
    let y = x;

    println!("x = {}, y = {}", x, y);

    let fruit = String::from("Apple");  // fruit comes into scope

    // ownership of fruit moves into the function
    print_fruit(fruit);

    // fruit is moved to the function so is no longer available here
    // error
    // println!("fruit = {}", fruit);    

    // number comes into scope
    let number = 10;

    // value of the number is copied into the function
    print_number(number);

    // number variable can be used here
    println!("number = {}", number);
}

fn print_fruit(str: String) {   // str comes into scope
    println!("str = {}", str);
}   // str goes out of scope and is dropped, plus memory is freed

fn print_number(value: i32) { // value comes into scope
    println!("value = {}", value);
}   // value goes out of scope