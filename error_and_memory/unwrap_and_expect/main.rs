// function to find a user by their username which returns an Option type
fn get_user(username: &str) -> Option<&str> {
    if username.is_empty() {
        return None;
    }

    return Some(username);
}

fn main() {
    // returns an Option
    let user_option = get_user("Hari");

    // use of match expression to get the result out of Option
    let result = match user_option {
        Some(user) => user,
        None => "not found!",
    };

    // print the result
    println!("user = {:?}", result);

    // use of unwrap method to get the result of Option enum from get_user function
    let result = get_user("Hari").unwrap();

    // print the result
    println!("user = {:?}", result);

    let result = get_user("").unwrap(); // thread 'main' panicked at 'called `Option::unwrap()` on a `None` value', src/main.rs:12:31ßß

    // use of expect method to get the result of Option enum from get_user function
    let result = get_user("").expect("fetch user"); // thread 'main' panicked at 'fetch user', src/main.rs:12:31

    // print the result
    println!("user = {:?}", result);
}