fn main() {
    // an array of numbers
    let numbers = [1, 2, 3, 4, 5];
    
    // create a slice of 2nd and 3rd element
    let slice = &numbers[1..3];
    
    println!("array = {:?}", numbers);
    println!("slice = {:?}", slice);

    let numbers = [1, 2, 3, 4, 5];

    // omit the start index
    let slice = &numbers[..3];

    println!("array = {:?}", numbers);
    println!("slice = {:?}", slice);

    let numbers = [1, 2, 3, 4, 5];

    // omit the end index
    let slice = &numbers[2..];

    println!("array = {:?}", numbers);
    println!("slice = {:?}", slice);

    let numbers = [1, 2, 3, 4, 5];
    
    // omit the start index and the end index
    // reference the whole array
    let slice = &numbers[..];

    println!("array = {:?}", numbers);
    println!("slice = {:?}", slice);

    // mutable array
    let mut colors = ["red", "green", "yellow", "white"];

    println!("array = {:?}", colors);

    // mutable slice
    let sliced_colors = &mut colors[1..3];
    
    println!("original slice = {:?}", sliced_colors);

    // change the value of the original slice at the first index
    sliced_colors[1] = "purple";

    println!("changed slice = {:?}", sliced_colors);

    let string = String::from("Hello World!");

    // slicing a string
    let slice = &string[0..5];

    println!("string = {}", string);
    println!("slice = {}", slice);

    let vector = vec!['A', 'E', 'I', 'O', 'U'];

    // slicing a vector
    let slice = &vector[1..4];

    println!("vector = {:?}", vector);
    println!("slice = {:?}", slice);
}