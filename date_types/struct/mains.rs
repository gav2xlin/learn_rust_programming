fn main() {
    // define a Person struct
    struct Person {
        name: String,
        age: u8,
        height: u8
    }

    // instantiate Person struct
    let person = Person {
        name: String::from("John Doe"),
        age: 18,
        height: 178
    };

    // access value of name field in Person struct
    println!("Person name = {}", person.name);

    // access value of age field in Person struct
    println!("Person age = {}", person.age);

    // access value of height field in Person struct
    println!("Person height = {}", person.height);

    // define a Person struct
    /*struct Person {
        name: String,
        age: u8,
        height: u8
    }*/
 
    // instantiate Person struct
    let person = Person {
        name: String::from("John Doe"),
        age: 18,
        height: 178
    };

    // destructure Person struct into name, age and height variables
    let Person { name, age, height } = person;

    println!("Person name = {}", name);
    println!("Person age = {}", age);
    println!("Person height = {}", height);

    // define a Point struct
    struct Point {
        x: i32,
        y: i32,
    }

    // instantiate Point struct to a mutable structure variable
    let mut point = Point { x: 0, y: 0 };

    println!("Before change:");
    println!("Point x = {}", point.x);
    println!("Point y = {}", point.y);

    // change the value of x field in mutable point struct
    point.x = 5;

    println!();
    println!("After change:");
    println!("Point x = {}", point.x);
    println!("Point y = {}", point.y);
}