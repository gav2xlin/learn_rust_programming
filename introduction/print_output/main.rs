fn main() {
    print!("Hello, World!");

    print!("Rust is fun! ");
    print!("I love Rust programming.");

    let age = 31;

    // print the variable using println!
    println!("{}", age);

    // print the variable using print!
    print!("{}", age);

    let age = 31;

    // print the variable using println!
    println!("Age = {}", age);

    let age = 31;
    let name = "Jack";

    // print the variables using println!
    println!("Name = {}, Age = {}", name, age);

    let age = 31;
    let name = "Jack";

    // print the variable using println!
    println!("Name = {0}, Age = {1}", name, age);

    let age = 31;
    let name = "Jack";

    // print the variables using println!
    println!("Name = {name}, Age = {age}");

    print!("Rust is fun!\nI love Rust programming.");
}