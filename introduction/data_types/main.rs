fn main() {
    // Signed integer type 
    let x: i32 = -200;
    let y: i32 = 200;

    println!("x = {}", x);
    println!("y = {}", y);

    // Unsigned integer type
    let x: u32 = 300;

    println!("x = {}", x);

    /*
    let x: u32 = -200; // cannot apply unary operator `-`
    println!("x = {}", x);
    */

    /*
    8-bit	i8	u8
    16-bit	i16	u16
    32-bit	i32	u32
    64-bit	i64	u64
    128-bit	i128	u128
    */

    let x: f32 = 3.1;

    // f32 floating point type
    let x: f32 = 3.1;

    // f64 floating point type
    let y: f64 = 45.0000031;

    println!("x = {}", x);
    println!("y = {}", y);

    // boolean type
    let flag1: bool = true;
    let flag2: bool = false;

    println!("flag1 = {}", flag1);
    println!("flag2 = {}", flag2);

    // char type
    let character: char = 'z';
    let special_character: char = '$';

    println!("character = {}", character);
    println!("special_character = {}", special_character);

    // Type Inference
    let x = 51;

    println!("x = {}", x);
}