fn main() {
    // variable to store integer value
    let age = 31;
    println!("Age: {}", age);

    // variable to store floating-point value
    let salary = 342523.23;
    println!("Salary: {}", salary);

    // variable to store string
    let name = "Jackie";
    println!("Name: {}", name);

    let age = 31;      // valid and good practice
    let _age = 31;    // valid variable 
    let 1age = 31;    // inavlid variable

    let age1 = 31;        // valid variable
    let age_num = 31;     // valid variable
    // let s@lary = 52352;   // invalid variable

    // let first name = "Jack";    // invalid variable
    let first_name = "Jack";    // valid variable
    // let first-name = "Jack";    // invalid variable

    // declare a variable with value 1
    let x = 1;
    println!("x = {}", x);

    // change the value of variable x
    x = 2; // cannot assign twice to immutable variable
    println!("x = {}", x);

    // declare a mutable variable with value 1
    let mut x = 1;
    println!("Value of x = {}", x);

    // change the value of variable x
    x = 2;
    println!("Updated value of x = {}", x);

    // declare a constant
    const PI:f32 = 3.14;
    println!("Initial Value of PI: {}", PI);

    // change value of PI
    PI = 535.23; // cannot assign to this expression
    println!("Update Value of PI: {}", PI);
}