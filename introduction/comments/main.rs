// entry point of the program
fn main() {
    // print text on the screen
    println!("Hello, World!");

    // declare a variable
    let x = 1;
    println!("x = {}", x);

    let x = 1;    // declare a variable
    println!("x = {}", x);

    /*
    declare a variable
    and assign value to it
    */
    let x = 1;
    println!("x = {}", x);

    // declare a variable
    // and assign value to it
    let x = 1;
    println!("x = {}", x);

    let x = 1;
    let y = 2;
    let z = 3;
    println!("z = {}", z);

    /*
    temporarily disable x and y variable declarations.
    let x = 1;
    let y = 2;
    */

    let z = 3;
    println!("z = {}", z);
}