fn foo() {
    let y = 999;
    let z = 333;
}

fn main() {
    let x = 111;
    
    foo();

    let x = Box::new(100);
    let y = 222;

    println!("x = {}, y = {}", x, y);
}