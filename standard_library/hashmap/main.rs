// import HashMap from Rust standard collections library
use std::collections::HashMap;

fn main() {
    // create a new HashMap
    let mut info: HashMap<i32, String> = HashMap::new();

    println!("HashMap = {:?}", info);

    let mut fruits: HashMap<i32, String> = HashMap::new();

    // insert elements to hashmap
    fruits.insert(1, String::from("Apple"));
    fruits.insert(2, String::from("Banana"));

    println!("fruits = {:?}", fruits);

    let mut fruits: HashMap<i32, String> = HashMap::new();

    // insert elements in a hashmap
    fruits.insert(1, String::from("Apple"));
    fruits.insert(2, String::from("Banana"));

    // access values in a hashmap
    let first_fruit = fruits.get(&1);
    let second_fruit = fruits.get(&2);
    let third_fruit = fruits.get(&3);

    println!("first fruit = {:?}", first_fruit);
    println!("second fruit = {:?}", second_fruit);
    println!("third fruit = {:?}", third_fruit);

    let mut fruits: HashMap<i32, String> = HashMap::new();

    // insert values in a hashmap
    fruits.insert(1, String::from("Apple"));
    fruits.insert(2, String::from("Banana"));

    println!("fruits before remove operation = {:?}", fruits);

    // remove value in a hashmap
    fruits.remove(&1);

    println!("fruits after remove operation = {:?}", fruits);

    let mut fruits: HashMap<i32, String> = HashMap::new();

    // insert values in a hashmap
    fruits.insert(1, String::from("Apple"));
    fruits.insert(2, String::from("Banana"));

    println!("Before update = {:?}", fruits);

    // change value of hashmap with key of 1
    fruits.insert(1, String::from("Mango"));

    println!("After update = {:?}", fruits);

    let mut fruits: HashMap<i32, String> = HashMap::new();

    fruits.insert(1, String::from("Apple"));
    fruits.insert(2, String::from("Banana"));

    // loop and print values of hashmap using values() method
    for fruit in fruits.values() {
        println!("{}", fruit)
    }

    // print the length of hashmap using len() method
    println!("Length of fruits = {}", fruits.len());
}