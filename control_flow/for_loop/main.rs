fn main() {
    // usage of for loop
    for i in 1..6 {
        println!("{}", i);
    }

    let mut sum = 0;
    
    // for loop to iterate over first 10 natural numbers
    for i in 1..11 {
        sum += i;
    }
    
    println!("Sum: {}", sum);

    /* "C-style" loop does not exist in Rust
    for (i = 0; i < 10; i++) {
        printf("%d\n", i);
    }
    */

    // for loop with inclusive range notation
    for i in 1..=5 {
        println!("{}", i);
    }

    let fruits = ["Apple", "Orange", "Banana"];
    
    // for loop to iterate through items in an array
    for fruit in fruits {
        println!("{}", fruit);
    }
}